package com.example.zuitt.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController

public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s!", name);
	}
	@GetMapping("/shape")
	public String shape(@RequestParam String shapeName, int sides){
		return String.format("Your shape is " + shapeName + ". It has " + sides + " sides");
	}
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "error") String name){
		return String.format("Hi " + name + "!");
	}
	@GetMapping("/nameage")
	public String nameage(@RequestParam String name, int age){
		return String.format("Hello " + name + "! Your age is " + age + ".");
	}



}
