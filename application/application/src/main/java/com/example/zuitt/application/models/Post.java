package com.example.zuitt.application.models;

import javax.persistence.*;

@Entity

@Table(name="posts")
public class Post {
    /*indicate that this property represents the primary key via @id*/
    @Id
    /*values for this property will be auto-incremented*/

    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    /*Default Constructors - needed when retrieving posts*/
    public Post (){}

    public Post (String title, String content){
        this.title = title;
        this.content = content;
    }

    /*Getters*/
    public String getTitle () {
        return title;
    }

    public String getContent () {
        return content;
    }

    /*Setters*/

    public void setTitle (String title) {
        this.title = title;
    }

    public void setContent (String content) {
        this.content = content;
    }
}
