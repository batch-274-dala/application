package com.example.zuitt.application.repositories;


import com.example.zuitt.application.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends CrudRepository<Post, Object> {

}
